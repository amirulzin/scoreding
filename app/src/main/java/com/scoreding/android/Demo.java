package com.scoreding.android;

import com.scoreding.android.model.composite.Classroom;
import com.scoreding.android.model.composite.Event;
import com.scoreding.android.model.composite.Homework;
import com.scoreding.android.model.composite.Parent;
import com.scoreding.android.model.composite.Student;
import com.scoreding.android.model.composite.Teacher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

/**
 * Main class for mock data
 */
public class Demo {

    private static final Random r = new Random();

    public static String getDemoEmail() {
        return "user@gmail.com";
    }

    public static String getDemoPassword() {
        return "password";
    }

    // imgur link for sample photos:
    // http://imgur.com/a/clNUp
    public static Parent createParentDemo() {
        return new Parent("Bakri Hassan", "700914-03-2457", "bakri@gmail.com", newChildrens(), "http://i.imgur.com/3VBGIIH.jpg");
    }

    private static List<Student> newChildrens() {
        ArrayList<Student> students = new ArrayList<Student>();
        Student studentA = getStudentA();

        Student studentB = getStudentB();

        Student studentC = getStudentC();

        students.add(studentA);
        students.add(studentB);
        students.add(studentC);
        return students;
    }

    private static Student getStudentC() {
        return new StudentBuilder()
                .setName("Khairi bin Bakri")
                .setIcNumber("9104021-04-0214")
                .setSchoolName("SMK Taman Indah")
                .setClassName("3 Pertiwi")
                .setAttendance("Present")
                .setHasReportCard(true)
                .setHomeroomTeacher("Sri Visvanathan")
                .setHomeworkStatus("Good")
                .setImageUrl("http://i.imgur.com/5jWVFgc.jpg")
                .setLatestComment("Need more focus on Math")
                .setNegativeFeedbacks(1)
                .setPositiveFeedbacks(4)
                .setHomeworks(Arrays.asList(new Homework("Chemistry", "Tuesday", null)))
                .createStudent();
    }

    private static Student getStudentB() {
        return new StudentBuilder()
                .setName("Zaman bin Bakri")
                .setIcNumber("9007010-05-8187")
                .setSchoolName("SMK Kubang Kerian")
                .setClassName("4 Meranti")
                .setAttendance("Present")
                .setHasReportCard(false)
                .setHomeroomTeacher("Wong Fei Li")
                .setHomeworkStatus("Good")
                .setImageUrl("http://i.imgur.com/R78b8LA.jpg")
                .setLatestComment("A dutiful student")
                .setNegativeFeedbacks(0)
                .setPositiveFeedbacks(8)
                .setHomeworks(Arrays.asList(new Homework("History", "Tuesday", null), new Homework("Physics", "Thursday", null)))
                .createStudent();
    }

    private static Student getStudentA() {
        return new StudentBuilder()
                .setName("Suhaila binti Bakri")
                .setIcNumber("890315-08-1129")
                .setSchoolName("SMK Kubang Pasu")
                .setClassName("5 Kasturi")
                .setAttendance("Present")
                .setHasReportCard(false)
                .setHomeroomTeacher("Zakri Herman")
                .setHomeworkStatus("Good")
                .setImageUrl("http://i.imgur.com/Z1C0WOW.jpg")
                .setLatestComment("A hardworking student")
                .setNegativeFeedbacks(0)
                .setPositiveFeedbacks(5)
                .setHomeworks(Arrays.asList(new Homework("Mathematic", "Friday", null), new Homework("Science", "Wednesday", null)))
                .createStudent();
    }

    /**
     * Create a student where basic field in original except name & classroom are filled from fields in backup
     */
    public static Student fuseMain(Student original, Student backup, String teacherName) {
        return new StudentBuilder(backup)
                .setName(original.getName())
                .setClassName(original.getClassroomName())
                .setHomeroomTeacher(teacherName)
                .createStudent();

    }

    public static Teacher getMainTeacher() {
        return getMainTeacher("Sri Visvanathan");
    }

    public static Teacher getMainTeacher(String teacherName) {
        List<Classroom> list = Arrays.asList(createRandomClassroom("3 Pertiwi", 20), createRandomClassroom("4 Perwira", 25), createRandomClassroom("4 Pemaju", 23));
        return new Teacher(teacherName, "700430-07-8440", "visvanathan@.edu.my", "3 Pertiwi", list, "http://i.imgur.com/Rm8Vq3L.jpg", "Mathematics");
    }


    private static Classroom createRandomClassroom(String classroomName, int studentCount) {
        List<Student> students = new ArrayList<>(studentCount);
        for (int i = 0; i < studentCount; i++) {
            students.add(createRandomStudentName(classroomName).createStudent());
        }
        return new Classroom(classroomName, students, Arrays.asList(getRandomSubject(), getRandomSubject()), getRandomTeacherSchedule());
    }

    private static List<String> getRandomTeacherSchedule() {
        String[] schedule = new String[]{"Mon", "Tue", "Wed", "Thu", "Fri"};
        List<String> out = new ArrayList<>(schedule.length);
        for (String s : schedule) {
            if (r.nextBoolean())
                out.add(s);
        }
        return out;
    }

    private static String getRandomSubject() {
        String[] subjects = new String[]{"Mathematics", "Physics", "Chemistry"};
        return subjects[r.nextInt(subjects.length)];
    }

    private static StudentBuilder createRandomStudentName(String classroomName) {
        String[] namePair = new String[]{"Afif", "Embung", "Ku", "Sharif", "Ahmad", "Salim", "Halim", "Mohd", "Engku", "Shah", "Farid"};
        String[] nameEnd = new String[]{"Jalani", "Karim", "Taib", "Zarif", "Razman", "Shafiq", "Othman", "Yusri", "Talib", "Akram"};


        return new StudentBuilder().setName(namePair[r.nextInt(namePair.length)] + " " + nameEnd[r.nextInt(nameEnd.length)]).setClassName(classroomName);
    }

    public static List<Event> getEvents() {
        ArrayList<Event> events = new ArrayList<>();
        events.add(new Event("Sports Day", "School-wide sporting event", getRandomDay()));
        events.add(new Event("Marathon for Charity", "Organized by Malaysian Heart Society", getRandomDay()));
        events.add(new Event("Parent Teacher Meeting", "Students report card can be retrieved today", getRandomDay()));
        events.add(new Event("District Debate Tournament", "District debate competition on current issues", getRandomDay()));
        events.add(new Event("Academic Achievement Day", "A day to celebrate academic laurels of graduating students", getRandomDay()));
        return events;
    }

    private static GregorianCalendar getRandomDay() {

        GregorianCalendar currCal = new GregorianCalendar();
        int currDay = currCal.get(Calendar.DAY_OF_MONTH);
        final GregorianCalendar nextMonthCal = new GregorianCalendar();
        nextMonthCal.roll(Calendar.MONTH, 1);

        int randDay = r.nextInt(27);
        if (r.nextBoolean()) {
            if (randDay <= currDay) {
                return getRandomDay();
            }
            currCal.set(Calendar.DAY_OF_MONTH, randDay);
            return currCal;
        } else {
            nextMonthCal.set(Calendar.DAY_OF_MONTH, randDay);
            return nextMonthCal;
        }
    }

    public static Student getMainStudent() {
        return getStudentB();
    }

    /**
     *
     */
    private static class StudentBuilder {
        private String uniqueId;
        private String name;
        private String icNumber;
        private String schoolName;
        private String homeroomTeacher;
        private String className;
        private String attendance;
        private int positiveFeedbacks;
        private int negativeFeedbacks;
        private boolean hasReportCard;
        private String homeworkStatus;
        private String latestComment;
        private String imageUrl;
        private List<Homework> homeworks;

        public StudentBuilder() {
        }

        public StudentBuilder(Student student) {
            this.setUniqueId(student.getUniqueId())
                    .setName(student.getName())
                    .setIcNumber(student.getIcNumber())
                    .setSchoolName(student.getSchoolName())
                    .setHomeroomTeacher(student.getHomeroomTeacher())
                    .setClassName(student.getClassroomName())
                    .setAttendance(student.getAttendanceStatus())
                    .setPositiveFeedbacks(student.getPositiveFeedbacks())
                    .setNegativeFeedbacks(student.getNegativeFeedbacks())
                    .setHasReportCard(student.hasReportCard())
                    .setHomeworkStatus(student.getHomeworkStatus())
                    .setLatestComment(student.getLatestComment())
                    .setImageUrl(student.getImageUrl())
                    .setHomeworks(student.getHomeworks());

        }

        public StudentBuilder setUniqueId(String uniqueId) {
            this.uniqueId = uniqueId;
            return this;
        }

        public StudentBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public StudentBuilder setIcNumber(String icNumber) {
            this.icNumber = icNumber;
            return this;
        }

        public StudentBuilder setSchoolName(String schoolName) {
            this.schoolName = schoolName;
            return this;
        }

        public StudentBuilder setHomeroomTeacher(String homeroomTeacher) {
            this.homeroomTeacher = homeroomTeacher;
            return this;
        }

        public StudentBuilder setClassName(String className) {
            this.className = className;
            return this;
        }

        public StudentBuilder setAttendance(String attendance) {
            this.attendance = attendance;
            return this;
        }

        public StudentBuilder setPositiveFeedbacks(int positiveFeedbacks) {
            this.positiveFeedbacks = positiveFeedbacks;
            return this;
        }

        public StudentBuilder setNegativeFeedbacks(int negativeFeedbacks) {
            this.negativeFeedbacks = negativeFeedbacks;
            return this;
        }

        public StudentBuilder setHasReportCard(boolean hasReportCard) {
            this.hasReportCard = hasReportCard;
            return this;
        }

        public StudentBuilder setHomeworkStatus(String homeworkStatus) {
            this.homeworkStatus = homeworkStatus;
            return this;
        }

        public StudentBuilder setLatestComment(String latestComment) {
            this.latestComment = latestComment;
            return this;
        }

        public StudentBuilder setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
            return this;
        }

        public StudentBuilder setHomeworks(List<Homework> homeworks) {
            this.homeworks = homeworks;
            return this;
        }

        public Student createStudent() {
            return new Student(uniqueId, name, icNumber, schoolName, homeroomTeacher, className, attendance, positiveFeedbacks, negativeFeedbacks, hasReportCard, homeworkStatus, latestComment, imageUrl, homeworks);
        }
    }
}
