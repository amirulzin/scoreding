package com.scoreding.android.model.composite;

import org.parceler.Parcel;

import java.util.List;

/**
 * Base model for student. Configured based on anemic model.
 */

@Parcel
public class Student {
    String uniqueId; //TODO MINOR: Student uniqueID can be changed to long if needed by backend implementation
    String name;
    String icNumber;
    String schoolName;
    String homeroomTeacher;
    String className;
    String attendance;
    int positiveFeedbacks;
    int negativeFeedbacks;
    boolean hasReportCard;
    String homeworkStatus;
    String latestComment;
    String imageUrl;
    List<Homework> homeworks;

    // For DEMO only. Not exactly used in proper implementation.
    public Student(String uniqueId, String name, String icNumber, String schoolName, String homeroomTeacher, String className, String attendance, int positiveFeedbacks, int negativeFeedbacks, boolean hasReportCard, String homeworkStatus, String latestComment, String imageUrl, List<Homework> homeworks) {
        this.uniqueId = uniqueId;
        this.name = name;
        this.icNumber = icNumber;
        this.schoolName = schoolName;
        this.homeroomTeacher = homeroomTeacher;
        this.className = className;
        this.attendance = attendance;
        this.positiveFeedbacks = positiveFeedbacks;
        this.negativeFeedbacks = negativeFeedbacks;
        this.hasReportCard = hasReportCard;
        this.homeworkStatus = homeworkStatus;
        this.latestComment = latestComment;
        this.imageUrl = imageUrl;
        this.homeworks = homeworks;
    }

    public Student(String name, String icNumber, String schoolName, String className) {
        this.name = name;
        this.icNumber = icNumber;
        this.schoolName = schoolName;
        this.className = className;
    }

    public Student() {
    }

    public List<Homework> getHomeworks() {
        return homeworks;
    }

    public String getLatestComment() {
        return latestComment;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getHomeroomTeacher() {
        return homeroomTeacher;
    }

    public String getHomeworkStatus() {
        return homeworkStatus;
    }

    public int getPositiveFeedbacks() {
        return positiveFeedbacks;
    }

    public int getNegativeFeedbacks() {
        return negativeFeedbacks;
    }

    public boolean hasReportCard() {
        return hasReportCard;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public String getName() {
        return name;
    }

    public String getIcNumber() {
        return icNumber;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getClassroomName() {
        return className;
    }

    public String getAttendanceStatus() {
        return attendance;
    }


    @Override
    public String toString() {
        return "Student{" +
                "uniqueId='" + uniqueId + '\'' +
                ", name='" + name + '\'' +
                ", icNumber='" + icNumber + '\'' +
                ", schoolName='" + schoolName + '\'' +
                ", homeroomTeacher='" + homeroomTeacher + '\'' +
                ", className='" + className + '\'' +
                ", attendance='" + attendance + '\'' +
                ", positiveFeedbacks=" + positiveFeedbacks +
                ", negativeFeedbacks=" + negativeFeedbacks +
                ", hasReportCard=" + hasReportCard +
                ", homeworkStatus='" + homeworkStatus + '\'' +
                ", latestComment='" + latestComment + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", homeworks=" + homeworks +
                '}';
    }
}
