package com.scoreding.android.model.composite;

import org.parceler.Parcel;

/**
 *
 */
@Parcel
public class Homework {
    String subject;
    String due;
    String notes;

    public Homework(String subject, String due, String notes) {
        this.subject = subject;
        this.due = due;
        this.notes = notes;
    }

    public Homework() {
    }

    public String getSubject() {
        return subject;
    }

    public String getDue() {
        return due;
    }

    public String getNotes() {
        return notes;
    }
}
