package com.scoreding.android.model.auth;

import android.support.annotation.NonNull;

/**
 * Login model for processing user (parent/teacher) login. Listener required for future OkHttp/Retrofit implement
 */

public class LoginModel {

    public void login(String email, String password, @NonNull Listener listener) { //TODO PRIORITY: LoginModel - Replace dummy onComplete
        listener.onComplete(true);
    }

    public void loginGoogle(String googleAuth, @NonNull Listener listener) {//TODO MINOR: LoginModel - Google Sign-in API
    }

    public void loginFacebook(String facebookAuth, @NonNull Listener listener) {//TODO MINOR: LoginModel - Facebook SDK Sign-in API
    }

    public interface Listener {
        void onComplete(boolean success);
    }
}
