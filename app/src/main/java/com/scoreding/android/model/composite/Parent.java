package com.scoreding.android.model.composite;

import org.parceler.Parcel;

import java.util.List;

/**
 *
 */
@Parcel
public class Parent {
    String name;
    String icNumber;
    String email;
    List<Student> students;
    String imageUrl;

    // For demo purpose
    public Parent(String name, String icNumber, String email, List<Student> students, String imageUrl) {
        this.name = name;
        this.icNumber = icNumber;
        this.email = email;
        this.students = students;
        this.imageUrl = imageUrl;
    }

    public Parent() {
    }

    public Parent(String name, String icNumber, String email, List<Student> students) {
        this.name = name;
        this.icNumber = icNumber;
        this.email = email;
        this.students = students;
    }

    public String getName() {
        return name;
    }

    public String getIcNumber() {
        return icNumber;
    }

    public String getEmail() {
        return email;
    }

    public List<Student> getStudents() {
        return students;
    }

    public String getImageUrl() {
        return imageUrl;
    }

}
