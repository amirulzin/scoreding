package com.scoreding.android.model.composite;

import org.parceler.Parcel;

import java.util.List;

/**
 *
 */
@Parcel
public class Teacher {

    String name;
    String icNumber;
    String email;
    String mainSubject;
    String mainClassroom;
    List<Classroom> classrooms;
    String imageUrl;

    public Teacher(String name, String icNumber, String email, String mainClassroom, List<Classroom> classrooms, String imageUrl, String mainSubject) {
        this.name = name;
        this.icNumber = icNumber;
        this.email = email;
        this.mainClassroom = mainClassroom;
        this.classrooms = classrooms;
        this.imageUrl = imageUrl;
        this.mainSubject = mainSubject;
    }

    public Teacher() {
    }

    public String getMainSubject() {
        return mainSubject;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getName() {
        return name;
    }

    public String getIcNumber() {
        return icNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getMainClassroom() {
        return mainClassroom;
    }

    public List<Classroom> getClassrooms() {
        return classrooms;
    }
}
