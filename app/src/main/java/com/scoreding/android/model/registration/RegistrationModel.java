package com.scoreding.android.model.registration;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.scoreding.android.model.composite.Student;

import java.util.List;

/**
 * Registration model for onboarding users (Teacher & Parents)
 */

public class RegistrationModel {
    public void registerParent(String icNumber, String email, String password, List<Student> students, @NonNull Listener listener) { //TODO PRIORITY: Implement signup with backend
        listener.onRegistrationFinished(true, null);
    }

    public void registerTeacher(String icNumber, String email, String password, String schoolName, List<String> className, @NonNull Listener listener) { //TODO PRIORITY: Implement signup with backend
        listener.onRegistrationFinished(true, null);
    }

    public interface Listener {
        void onRegistrationFinished(boolean success, @Nullable String errorText);
    }
}
