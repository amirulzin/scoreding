package com.scoreding.android.model.composite;

import org.parceler.Parcel;

/**
 *
 */

@Parcel
public class Comments {
    String id;
    long date;
    String comment;
    String authorName;

    public Comments(long date, String comment, String authorName) {
        this.date = date;
        this.comment = comment;
        this.authorName = authorName;
    }

    public Comments() {
    }

    public Comments(String id, long date, String comment, String authorName) {
        this.id = id;
        this.date = date;
        this.comment = comment;
        this.authorName = authorName;
    }

    public String getId() {
        return id;
    }

    public long getDate() {
        return date;
    }

    public String getComment() {
        return comment;
    }

    public String getAuthorName() {
        return authorName;
    }
}
