package com.scoreding.android.model.composite;

import org.parceler.Parcel;

import java.util.Iterator;
import java.util.List;

/**
 *
 */
@Parcel
public class Classroom {
    String className;
    List<Student> students;
    List<String> subjects;
    List<String> classSchedule;

    public Classroom(String className, List<Student> students, List<String> subjects, List<String> classSchedule) {
        this.className = className;
        this.students = students;
        this.subjects = subjects;
        this.classSchedule = classSchedule;
    }

    public Classroom(String className, List<Student> students) {
        this.className = className;
        this.students = students;
    }

    public Classroom() {
    }

    public List<String> getSubjects() {
        return subjects;
    }

    public String getMainSubject() {
        return getSubjects().get(0);
    }

    public List<String> getClassSchedule() {
        return classSchedule;
    }

    public String getClassScheduleAsText() {
        StringBuilder b = new StringBuilder();
        for (Iterator<String> iterator = classSchedule.iterator(); iterator.hasNext(); ) {
            String s = iterator.next();
            b.append(s);
            if (iterator.hasNext()) {
                b.append(" | ");
            }
        }
        return b.toString();
    }

    public String getClassName() {
        return className;
    }

    public List<Student> getStudents() {
        return students;
    }
}
