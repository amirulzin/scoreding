package com.scoreding.android.model.composite;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 *
 */
@Parcel
public class Event {
    String title;
    String details;
    String venue;
    String eventType;
    long date;

    public Event(String title, String details, String venue, long date) {
        this.title = title;
        this.details = details;
        this.venue = venue;
        this.date = date;
    }

    public Event(String title, String details, String venue, String eventType, long date) {
        this.title = title;
        this.details = details;
        this.venue = venue;
        this.eventType = eventType;
        this.date = date;
    }

    public Event() {
    }

    public Event(String title, String details, long date) {
        this.title = title;
        this.details = details;
        this.date = date;
    }

    public Event(String title, String details, GregorianCalendar cal) {
        this.title = title;
        this.details = details;
        this.date = cal.getTimeInMillis();

    }

    public static List<Date> datesFrom(List<Event> events) {
        ArrayList<Date> out = new ArrayList<>(events.size());
        for (Event event : events) {
            out.add(new Date(event.date));
        }
        return out;
    }

    public String getEventType() {
        return eventType;
    }

    public String getVenue() {

        return venue;
    }

    public String getTitle() {
        return title;
    }

    public String getDetails() {
        return details;
    }

    public long getDateLong() {
        return date;
    }

    public Date getDate() {
        return new Date(date);
    }

    public GregorianCalendar getCalendar() {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(date);
        return calendar;
    }
}
