package com.scoreding.android.model.auth;

/**
 * Simple tuple for pair of strings
 */

public class StringTuple {
    private final String key;
    private final String value;

    public StringTuple(final String key, final String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

}
