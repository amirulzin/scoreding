package com.scoreding.android.view.common;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.scoreding.android.R;
import com.scoreding.android.databinding.StudentReportBinding;
import com.scoreding.android.model.composite.Student;
import com.scoreding.android.view.common.core.BaseBindingActivity;

import org.parceler.Parcels;

/**
 *
 */

public class ReportCardActivity extends BaseBindingActivity<StudentReportBinding> {
    private static final String KEY_STUDENT = "STUDENT";

    public static Intent makeIntent(Context context, @NonNull Student student) {
        Intent intent = new Intent(context, ReportCardActivity.class);
        intent.putExtra(KEY_STUDENT, Parcels.wrap(student));
        return intent;
    }

    @Override
    protected int getBaseLayoutId() {
        return R.layout.student_report;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Student student = Parcels.unwrap(getIntent().getParcelableExtra(KEY_STUDENT));

        StudentReportBinding b = getBaseBinding();
        b.examName.setText("Mid-term Examination");
        b.studentName.setText(student.getName());
        b.studentClassroom.setText(student.getClassroomName());
    }
}
