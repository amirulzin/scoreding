package com.scoreding.android.view.teacher;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import com.scoreding.android.Demo;
import com.scoreding.android.R;
import com.scoreding.android.view.common.AnnouncementFragment;
import com.scoreding.android.view.common.CalendarFragment;
import com.scoreding.android.view.common.core.BaseNavigationActivity;

import java.util.HashMap;

public class TeacherHomeActivity extends BaseNavigationActivity {
    private final HashMap<Integer, String> fragmentTagsMap = new HashMap<Integer, String>(4, 1f) {
        {
            put(R.id.navigationHome, TeacherProfileFragment.class.getSimpleName());
            put(R.id.navigationRecords, RecordsFragment.class.getSimpleName());
            put(R.id.navigationPlanner, CalendarFragment.class.getSimpleName());
            put(R.id.navigationAnnouncement, AnnouncementFragment.class.getSimpleName());
        }
    };


    @Override
    protected Fragment getInitialFragment() {
        return TeacherProfileFragment.newInstance(Demo.getMainTeacher(), true, true);
    }

    @Override
    protected int getMenuResId() {
        return R.menu.navigation_teacher;
    }

    @Nullable
    @Override
    protected Fragment getFragment(MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.navigationHome:
                fragment = TeacherProfileFragment.newInstance(Demo.getMainTeacher(), true, true);
                break;
            case R.id.navigationRecords:
                fragment = RecordsFragment.newInstance();
                break;
            case R.id.navigationPlanner:
                fragment = CalendarFragment.newInstance(Demo.getEvents());
                break;
            case R.id.navigationAnnouncement:
                fragment = AnnouncementFragment.newInstance(Demo.getEvents(), true);
                break;
        }

        return fragment;
    }

    @NonNull
    @Override
    protected String getFragmentTag(MenuItem item) {
        return fragmentTagsMap.get(item.getItemId());
    }

    @NonNull
    @Override
    protected String getInitialFragmentTag() {
        return fragmentTagsMap.get(R.id.navigationHome);
    }

}
