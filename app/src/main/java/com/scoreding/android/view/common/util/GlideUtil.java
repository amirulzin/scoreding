package com.scoreding.android.view.common.util;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * Standardizes Glide calls
 */

public class GlideUtil {

    public static <V extends ImageView> void loadProfileImage(Context context, String url, V imageView) {
        int radius = ResourceUtil.getPixels(context, 4);
        int margin = ResourceUtil.getPixels(context, 4);
        RoundedCornersTransformation transformation = new RoundedCornersTransformation(context, radius, margin);
        Glide.with(context).load(url).bitmapTransform(transformation).crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
    }
}
