package com.scoreding.android.view.teacher;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.scoreding.android.Demo;
import com.scoreding.android.R;
import com.scoreding.android.databinding.TeacherActivityClassroomBinding;
import com.scoreding.android.databinding.TeacherListItemStudentGridBinding;
import com.scoreding.android.databinding.TeacherListItemStudentHorizontalBinding;
import com.scoreding.android.model.composite.Classroom;
import com.scoreding.android.model.composite.Student;
import com.scoreding.android.view.common.core.BaseBindingActivity;
import com.scoreding.android.view.common.util.BindingHolder;
import com.scoreding.android.view.common.util.BindingItemAdapter;
import com.scoreding.android.view.common.util.StringUtil;
import com.scoreding.android.view.student.StudentPublicProfileActivity;

import org.parceler.Parcels;

import java.util.List;

/**
 *
 */

public class ClassroomActivity extends BaseBindingActivity<TeacherActivityClassroomBinding> {
    private static final String KEY_STUDENTS = "STUDENTS";

    public static void launchActivity(Context context, Classroom classroom) {
        Intent out = new Intent(context, ClassroomActivity.class);
        out.putExtra(KEY_STUDENTS, Parcels.wrap(classroom));
        context.startActivity(out);
    }

    @Override
    protected int getBaseLayoutId() {
        return R.layout.teacher_activity_classroom;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Classroom classroom = Parcels.unwrap(getIntent().getParcelableExtra(KEY_STUDENTS));
        final TeacherActivityClassroomBinding b = getBaseBinding();
        b.toolbar.setTitle(classroom.getClassName());
        b.toolbar.setSubtitle(classroom.getMainSubject());


        final Adapter adapter = new Adapter(classroom.getStudents());
        adapter.setAdapterListener(new Adapter.AdapterListener() {
            @Override
            public void onSelectionModeToggle(boolean enabled) {
                b.studentActions.getRoot().setVisibility(View.VISIBLE);
            }

            @Override
            public View.OnClickListener onStudentClicked(final Student student, final int position, final boolean isSelectionMode) {
                return new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!isSelectionMode) {
                            Student fuse = Demo.fuseMain(student, Demo.getMainStudent(), Demo.getMainTeacher().getName());
                            StudentPublicProfileActivity.launchNew(ClassroomActivity.this, fuse);
                        }
                    }
                };
            }
        });

        b.studentActions.actionCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.toggleSelectionMode(false);
                b.studentActions.getRoot().setVisibility(View.GONE);
            }
        });

        RecyclerView rv = b.recyclerView;
        rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rv.setAdapter(adapter);
        TeacherActionHelper.applyAction(this, b.studentActions, Demo.getMainTeacher(), classroom);

    }

    /**
     * Image click = selection mode. Card click = Student details.
     */
    public static class Adapter extends BindingItemAdapter<Student> {

        private final SparseBooleanArray selection = new SparseBooleanArray(getItemCount());
        boolean gridMode = false;
        boolean selectionMode = false;
        private AdapterListener adapterListener;

        public Adapter(List<Student> list) {
            super(list);
        }

        public void setAdapterListener(AdapterListener adapterListener) {
            this.adapterListener = adapterListener;
        }

        public boolean isSelectionMode() {
            return selectionMode;
        }

        public void refreshList() {
            notifyItemRangeChanged(0, getItemCount());
        }

        public void useGrid(boolean enabled) {
            gridMode = enabled;
            refreshList();
        }

        public void toggleSelectionMode(boolean enabled) {
            selection.clear();
            selectionMode = enabled;
            refreshList();
        }

        @Override
        public int getItemViewType(int position) {
            return gridMode ? R.layout.teacher_list_item_student_grid : R.layout.teacher_list_item_student_horizontal;
        }

        @Override
        public BindingHolder<? extends ViewDataBinding> onCreateBindingHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
            return new BindingHolder<>(DataBindingUtil.inflate(inflater, viewType, parent, false));
        }

        @Override
        public void onBind(BindingHolder<? extends ViewDataBinding> holder, Student item, final int position) { //TODO Minor: Classroom - Add onCheckedChange for selected students
            ViewDataBinding binding = holder.getBinding();

            if (binding instanceof TeacherListItemStudentHorizontalBinding) {
                TeacherListItemStudentHorizontalBinding b = (TeacherListItemStudentHorizontalBinding) binding;
                b.studentName.setText(item.getName());
                b.checkBox.setVisibility(selectionMode ? View.VISIBLE : View.GONE);
                b.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (selectionMode)
                            selection.put(position, isChecked);
                    }
                });
                b.smileyPositive.setText(StringUtil.getEmojiSmile());
                b.smileyNegative.setText(StringUtil.getEmojiSad());

                if (selectionMode)
                    b.checkBox.setChecked(selection.get(position));
                else b.checkBox.setChecked(false);

                b.image.setOnClickListener(enableSelectionMode(position, b.itemBase));
                if (adapterListener != null)
                    b.cardBase.setOnClickListener(adapterListener.onStudentClicked(item, position, selectionMode));

            } else if (binding instanceof TeacherListItemStudentGridBinding) {
                TeacherListItemStudentGridBinding b = (TeacherListItemStudentGridBinding) binding;
                b.studentName.setText(item.getName());
                b.checkBox.setVisibility(selectionMode ? View.VISIBLE : View.GONE);
                //TODO Minor: Classroom - Fully implement grid items binding
            }
        }

        private View.OnClickListener enableSelectionMode(final int position, final View view) {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selectionMode) {
                        boolean selected = selection.get(position);

                        if (!selected) {
                            selection.put(position, true);
                        } else {
                            selection.put(position, false);
                        }
                        notifyItemChanged(position);
                    } else {
                        toggleSelectionMode(true);
                        selection.put(position, true);
                        if (adapterListener != null) {
                            adapterListener.onSelectionModeToggle(true);
                        }
                    }
                }
            };
        }

        public interface AdapterListener {
            void onSelectionModeToggle(boolean enabled);

            View.OnClickListener onStudentClicked(Student student, int position, boolean isSelectionMode);
        }
    }


}
