package com.scoreding.android.view.common.util;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

/**
 * T for any model objects. Try instantiating via constructor to understand the flow.
 */

public abstract class BindingItemAdapter<T> extends RecyclerView.Adapter<BindingHolder<? extends ViewDataBinding>> {

    private final List<T> list;

    public BindingItemAdapter(final List<T> list) {
        this.list = list;
    }

    public List<T> getList() {
        return list;
    }

    @Override
    public BindingHolder<? extends ViewDataBinding> onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return onCreateBindingHolder(LayoutInflater.from(parent.getContext()), parent, viewType);
    }

    public abstract BindingHolder<? extends ViewDataBinding> onCreateBindingHolder(LayoutInflater inflater, ViewGroup parent, final int viewType);

    @Override
    public void onBindViewHolder(final BindingHolder<? extends ViewDataBinding> holder, final int position) {
        onBind(holder, list.get(position), position);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public abstract void onBind(final BindingHolder<? extends ViewDataBinding> holder, final T item, final int position);

    public T getDataAt(final BindingHolder<? extends ViewDataBinding> holder) {
        return list.get(holder.getAdapterPosition());
    }
}
