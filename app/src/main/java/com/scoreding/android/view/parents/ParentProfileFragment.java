package com.scoreding.android.view.parents;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scoreding.android.R;
import com.scoreding.android.databinding.ParentFragmentProfileBinding;
import com.scoreding.android.databinding.ParentListItemChildrenBinding;
import com.scoreding.android.model.composite.Parent;
import com.scoreding.android.model.composite.Student;
import com.scoreding.android.view.common.ReportCardActivity;
import com.scoreding.android.view.common.core.BaseFragment;
import com.scoreding.android.view.common.util.BindingHolder;
import com.scoreding.android.view.common.util.BindingItemAdapter;
import com.scoreding.android.view.common.util.GlideUtil;
import com.scoreding.android.view.common.util.ItemClickSupport;
import com.scoreding.android.view.common.util.RecyclerViewUtil;
import com.scoreding.android.view.common.util.ResourceUtil;
import com.scoreding.android.view.common.util.StringUtil;
import com.scoreding.android.view.student.StudentPublicProfileActivity;

import org.parceler.Parcels;

import java.util.List;

/**
 *
 */
public class ParentProfileFragment extends BaseFragment<ParentFragmentProfileBinding> {

    private static final String KEY = "parent-parcel";

    public static ParentProfileFragment newInstance(Parent parent) {
        Bundle args = new Bundle();
        args.putParcelable(KEY, Parcels.wrap(parent));
        ParentProfileFragment fragment = new ParentProfileFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.parent_fragment_profile;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Parent parent = Parcels.unwrap(getArguments().getParcelable(KEY));

        ParentFragmentProfileBinding binding = getBinding();
        binding.profileName.setText(parent.getName());

        GlideUtil.loadProfileImage(getActivity(), parent.getImageUrl(), binding.profileImage);

        RecyclerView rv = binding.childrenRecyclerView;
        rv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rv.addItemDecoration(new RecyclerViewUtil.VerticalItemDecoration(ResourceUtil.getPixels(getActivity(), 8f)));
        rv.setAdapter(new Adapter(getActivity(), parent.getStudents()));

        ItemClickSupport.addTo(rv).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Student student = parent.getStudents().get(position);
                StudentPublicProfileActivity.launchNew(getActivity(), student);

            }
        });
    }


    public static class Adapter extends BindingItemAdapter<Student> {

        private final Context context;

        public Adapter(Context context, List<Student> collection) {
            super(collection);
            this.context = context;
        }

        @Override
        public BindingHolder<? extends ViewDataBinding> onCreateBindingHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
            ParentListItemChildrenBinding inflate = DataBindingUtil.inflate(inflater, R.layout.parent_list_item_children, parent, false);
            return new BindingHolder<>(inflate);
        }

        @Override
        public void onBind(BindingHolder<? extends ViewDataBinding> holder, final Student item, int position) {
            ViewDataBinding b = holder.getBinding();
            if (b instanceof ParentListItemChildrenBinding) {
                ParentListItemChildrenBinding binding = (ParentListItemChildrenBinding) b;

                // Can use binding.setVariable if we set it in layout
                // but didn't for now as layout may changed drastically in the future.
                binding.childName.setText(item.getName());
                binding.childAttendance.setText(item.getAttendanceStatus());
                binding.childSchool.setText(item.getSchoolName());
                binding.smileyPositive.setText(StringUtil.getEmojiSmile() + " " + String.valueOf(item.getPositiveFeedbacks()));
                binding.smileyNegative.setText(StringUtil.getEmojiSad() + " " + String.valueOf(item.getNegativeFeedbacks()));
                binding.reportCardAlert.setVisibility(item.hasReportCard() ? View.VISIBLE : View.GONE);

                binding.reportCardAlert.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.startActivity(ReportCardActivity.makeIntent(context, item));
                    }
                });
            }
        }
    }
}
