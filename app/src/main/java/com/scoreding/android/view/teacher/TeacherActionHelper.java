package com.scoreding.android.view.teacher;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.scoreding.android.R;
import com.scoreding.android.databinding.TeacherActionsStudentBinding;
import com.scoreding.android.model.composite.Classroom;
import com.scoreding.android.model.composite.Event;
import com.scoreding.android.model.composite.Teacher;

public class TeacherActionHelper {
    public static void applyAction(final Activity context, TeacherActionsStudentBinding binding, Teacher teacher, final Classroom classroom) {
        binding.actionReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Event event = new Event("Homework", classroom.getMainSubject(), classroom.getClassName(), System.currentTimeMillis());
                Intent intent = EditEventActivity.createActivityIntent(context, event);
                context.startActivity(intent);
            }
        });

        binding.actionAbsent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Students marked as absent", Toast.LENGTH_LONG).show();

            }
        });

        binding.actionComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialDialog b = new MaterialDialog.Builder(context).autoDismiss(true).customView(R.layout.module_add_comment, false).positiveText("Submit").negativeText("Cancel").build();
                b.show();
            }
        });
    }
}
