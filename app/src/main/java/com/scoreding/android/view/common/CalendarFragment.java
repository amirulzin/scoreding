package com.scoreding.android.view.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.scoreding.android.R;
import com.scoreding.android.databinding.ModuleCalendarBinding;
import com.scoreding.android.model.composite.Event;
import com.scoreding.android.view.common.core.BaseFragment;
import com.squareup.timessquare.CalendarPickerView;

import org.parceler.Parcels;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 *
 */

public class CalendarFragment extends BaseFragment<ModuleCalendarBinding> {
    private static final String KEY_EVENTS = "EVENTS";

    public static CalendarFragment newInstance(List<Event> events) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_EVENTS, Parcels.wrap(events));
        CalendarFragment fragment = new CalendarFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final List<Event> events = Parcels.unwrap(getArguments().getParcelable(KEY_EVENTS));

        CalendarPickerView calendar = getBinding().calendar;

        calendar.init(new Date(System.currentTimeMillis()), new Date(TimeUnit.DAYS.toMillis(365) + System.currentTimeMillis()), Locale.ENGLISH)
                .inMode(CalendarPickerView.SelectionMode.SINGLE)
                .withHighlightedDates(Event.datesFrom(events));


        calendar.setCellClickInterceptor(new CalendarPickerView.CellClickInterceptor() {
            @Override
            public boolean onCellClicked(Date date) {
                GregorianCalendar cal = new GregorianCalendar();
                cal.setTime(date);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                int month = cal.get(Calendar.MONTH);
                boolean found = false;
                for (Event event : events) {
                    GregorianCalendar eventCalendar = event.getCalendar();
                    if (eventCalendar.get(Calendar.MONTH) == month && eventCalendar.get(Calendar.DAY_OF_MONTH) == day) {
                        getBinding().eventContainer.setVisibility(View.VISIBLE);
                        getBinding().eventTitle.setText(event.getTitle());
                        getBinding().eventDetails.setText(event.getDetails());
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    getBinding().eventContainer.setVisibility(View.GONE);
                }
                return false;
            }
        });


    }

    @Override
    protected int getLayoutId() {
        return R.layout.module_calendar;
    }
}
