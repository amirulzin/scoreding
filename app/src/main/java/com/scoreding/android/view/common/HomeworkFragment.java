package com.scoreding.android.view.common;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.scoreding.android.R;
import com.scoreding.android.databinding.CommonFragmentRecyclerViewBinding;
import com.scoreding.android.databinding.ModuleListItemHeaderBinding;
import com.scoreding.android.databinding.ModuleListItemHomeworkBinding;
import com.scoreding.android.model.composite.Homework;
import com.scoreding.android.model.composite.Student;
import com.scoreding.android.view.common.core.BaseFragment;
import com.scoreding.android.view.common.util.BindingHolder;
import com.scoreding.android.view.common.util.BindingItemAdapter;
import com.scoreding.android.view.common.util.FlatData;

import org.parceler.Parcels;

import java.util.List;

/**
 *
 */

public class HomeworkFragment extends BaseFragment<CommonFragmentRecyclerViewBinding> {
    private static final String KEY_STUDENTS = "STUDENTS";

    public static HomeworkFragment newInstance(List<Student> students) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_STUDENTS, Parcels.wrap(students));
        HomeworkFragment fragment = new HomeworkFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.common_fragment_recycler_view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        List<Student> students = Parcels.unwrap(getArguments().getParcelable(KEY_STUDENTS));

        CommonFragmentRecyclerViewBinding b = getBinding();
        b.title.setText("Homeworks");

        RecyclerView rv = b.recyclerView;
        rv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rv.setAdapter(new Adapter(FlatData.getCollection(students, new FlatData.Factory<Student, Homework>() {
            @Override
            public String getName(Student group) {
                return group.getName();
            }

            @Override
            public List<Homework> getSubgroupList(Student student) {
                return student.getHomeworks();
            }
        })));
    }


    public static class Adapter extends BindingItemAdapter<FlatData<Student, Homework>> {

        public Adapter(List<FlatData<Student, Homework>> list) {
            super(list);
        }

        @Override
        public int getItemViewType(int position) {
            return getList().get(position).isHeader() ? R.layout.module_list_item_header : R.layout.module_list_item_homework;
        }

        @Override
        public BindingHolder<? extends ViewDataBinding> onCreateBindingHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {

            //Can we even simplify below? not sure if type will be erased out and break instanceof in onBind()
//            if (viewType == R.layout.module_list_item_header) {
//                ModularListItemHeaderBinding inflate = DataBindingUtil.inflate(inflater, viewType, parent, false);
//                return new BindingHolder<>(inflate);
//            } else {
//                ModularListItemHomeworkBinding inflate = DataBindingUtil.inflate(inflater, viewType, parent, false);
//                return new BindingHolder<>(inflate);
//            }

            //Edit: It actually works lol
            return new BindingHolder<>(DataBindingUtil.inflate(inflater, viewType, parent, false));
        }

        @Override
        public void onBind(BindingHolder<? extends ViewDataBinding> holder, FlatData<Student, Homework> item, int position) {
            ViewDataBinding binding = holder.getBinding();

            if (binding instanceof ModuleListItemHeaderBinding) {
                ((ModuleListItemHeaderBinding) binding).setHeader(item.getName());
            } else if (binding instanceof ModuleListItemHomeworkBinding) {
                ModuleListItemHomeworkBinding b = (ModuleListItemHomeworkBinding) binding;
                b.subject.setText(item.getSubgroup().getSubject());
                b.dueDate.setText("Due: Next ".concat(item.getSubgroup().getDue()));
            }
        }


    }
}
