package com.scoreding.android.view.teacher;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.scoreding.android.Demo;
import com.scoreding.android.R;
import com.scoreding.android.databinding.StudentProfileBinding;
import com.scoreding.android.databinding.TeacherActivityRecordsBinding;
import com.scoreding.android.model.composite.Student;
import com.scoreding.android.view.common.core.BaseFragment;
import com.scoreding.android.view.student.StudentProfileBindingHelper;

/**
 *
 */

public class RecordsFragment extends BaseFragment<TeacherActivityRecordsBinding> {

    public static RecordsFragment newInstance() {

        Bundle args = new Bundle();

        RecordsFragment fragment = new RecordsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.teacher_activity_records;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        StudentProfileBinding inc = getBinding().innerInclude;
        Student student = Demo.createParentDemo().getStudents().get(2);
        StudentProfileBindingHelper.applyStudent(getActivity(), inc, student, false);

    }
}
