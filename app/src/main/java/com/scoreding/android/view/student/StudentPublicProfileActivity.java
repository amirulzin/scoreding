package com.scoreding.android.view.student;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.scoreding.android.R;
import com.scoreding.android.databinding.StudentProfileBinding;
import com.scoreding.android.model.composite.Student;
import com.scoreding.android.view.common.core.BaseBindingActivity;

import org.parceler.Parcels;

/**
 *
 */

public class StudentPublicProfileActivity extends BaseBindingActivity<StudentProfileBinding> {

    private static final String KEY_STUDENT = "STUDENT";

    public static void launchNew(Context context, Student student) {

        Intent intent = new Intent(context, StudentPublicProfileActivity.class);
        intent.putExtra(KEY_STUDENT, Parcels.wrap(student));
        context.startActivity(intent);
    }

    @Override
    protected int getBaseLayoutId() {
        return R.layout.student_profile;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Student student = Parcels.unwrap(getIntent().getExtras().getParcelable(KEY_STUDENT));
        StudentProfileBinding binding = getBaseBinding();
        StudentProfileBindingHelper.applyStudent(this, binding, student, false);
    }

}
