package com.scoreding.android.view.student;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.scoreding.android.R;
import com.scoreding.android.databinding.StudentProfileBinding;
import com.scoreding.android.model.composite.Student;
import com.scoreding.android.view.common.core.BaseFragment;

import org.parceler.Parcels;

/**
 *
 */

public class StudentProfileFragment extends BaseFragment<StudentProfileBinding> {

    private static final String KEY_STUDENT = "STUDENT";

    public static StudentProfileFragment newInstance(Student student) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_STUDENT, Parcels.wrap(student));
        StudentProfileFragment fragment = new StudentProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.student_profile;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Student student = Parcels.unwrap(getArguments().getParcelable(KEY_STUDENT));
        StudentProfileBindingHelper.applyStudent(getActivity(), getBinding(), student, true);
    }
}
