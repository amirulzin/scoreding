package com.scoreding.android.view.student;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import com.scoreding.android.Demo;
import com.scoreding.android.R;
import com.scoreding.android.model.composite.Student;
import com.scoreding.android.view.common.AnnouncementFragment;
import com.scoreding.android.view.common.CalendarFragment;
import com.scoreding.android.view.common.HomeworkFragment;
import com.scoreding.android.view.common.core.BaseNavigationActivity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 */

public class StudentHomeActivity extends BaseNavigationActivity {

    private static final HashMap<Integer, String> fragmentTag = new HashMap<Integer, String>() {
        {
            put(R.id.navigationHome, StudentProfileFragment.class.getSimpleName());
            put(R.id.navigationHomework, HomeworkFragment.class.getSimpleName());
            put(R.id.navigationPlanner, CalendarFragment.class.getSimpleName());
            put(R.id.navigationAnnouncement, AnnouncementFragment.class.getSimpleName());
        }
    };

    @Override
    protected Fragment getInitialFragment() {
        return StudentProfileFragment.newInstance(Demo.getMainStudent());
    }

    @Override
    protected int getMenuResId() {
        return R.menu.navigation_student;
    }

    @Nullable
    @Override
    protected Fragment getFragment(MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.navigationHome:
                fragment = StudentProfileFragment.newInstance(Demo.getMainStudent());
                break;
            case R.id.navigationHomework:
                ArrayList<Student> list = new ArrayList<>();
                list.add(Demo.getMainStudent());
                fragment = HomeworkFragment.newInstance(list);
                break;
            case R.id.navigationPlanner:
                fragment = CalendarFragment.newInstance(Demo.getEvents());
                break;
            case R.id.navigationAnnouncement:
                fragment = AnnouncementFragment.newInstance(Demo.getEvents(), false);
                break;
        }

        return fragment;
    }


    @NonNull
    @Override
    protected String getFragmentTag(MenuItem item) {
        return fragmentTag.get(item.getItemId());
    }

    @NonNull
    @Override
    protected String getInitialFragmentTag() {
        return StudentProfileFragment.class.getSimpleName();
    }


}
