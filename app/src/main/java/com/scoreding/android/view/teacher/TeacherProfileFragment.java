package com.scoreding.android.view.teacher;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scoreding.android.R;
import com.scoreding.android.databinding.TeacherFragmentProfileBinding;
import com.scoreding.android.databinding.TeacherListItemClassroomBinding;
import com.scoreding.android.model.composite.Classroom;
import com.scoreding.android.model.composite.Teacher;
import com.scoreding.android.view.common.core.BaseFragment;
import com.scoreding.android.view.common.util.BindingHolder;
import com.scoreding.android.view.common.util.BindingItemAdapter;
import com.scoreding.android.view.common.util.GlideUtil;
import com.scoreding.android.view.common.util.ItemClickSupport;

import org.parceler.Parcels;

import java.util.List;

/**
 *
 */

public class TeacherProfileFragment extends BaseFragment<TeacherFragmentProfileBinding> {
    private static final String KEY_TEACHER = "TEACHER";
    private static final String KEY_BOOL_CLASSROOM_DETAIL = "CLASSROOM_DETAIL";
    private static final String KEY_BOOL_ENABLE_FAB = "ENABLE_FAB";

    public static TeacherProfileFragment newInstance(Teacher teacher, boolean enableClassroomDetail, boolean enableFab) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_TEACHER, Parcels.wrap(teacher));
        args.putBoolean(KEY_BOOL_CLASSROOM_DETAIL, enableClassroomDetail);
        args.putBoolean(KEY_BOOL_ENABLE_FAB, enableFab);
        TeacherProfileFragment fragment = new TeacherProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnFabClick(View.OnClickListener onClickListener) {
        getBinding().profileFab.setOnClickListener(onClickListener);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.teacher_fragment_profile;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Teacher teacher = Parcels.unwrap(getArguments().getParcelable(KEY_TEACHER));
        TeacherFragmentProfileBinding b = getBinding();

        GlideUtil.loadProfileImage(getActivity(), teacher.getImageUrl(), b.profileImage);

        b.profileName.setText(teacher.getName());
        b.profileClassroom.setText(teacher.getMainClassroom());
        b.profileSubject.setText(teacher.getMainSubject());

        b.profileFab.setVisibility(getArguments().getBoolean(KEY_BOOL_ENABLE_FAB, false) ? View.VISIBLE : View.GONE);

        RecyclerView rv = b.classroomRecyclerView;
        rv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        final Adapter adapter = new Adapter(teacher.getClassrooms());
        rv.setAdapter(adapter);
        if (getArguments().getBoolean(KEY_BOOL_CLASSROOM_DETAIL, false)) {
            ItemClickSupport.addTo(rv).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                    ClassroomActivity.launchActivity(getActivity(), adapter.getList().get(position));
                }
            });
        }
    }

    public static class Adapter extends BindingItemAdapter<Classroom> {

        public Adapter(List<Classroom> list) {
            super(list);
        }

        @Override
        public BindingHolder<? extends ViewDataBinding> onCreateBindingHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
            return new BindingHolder<>(DataBindingUtil.inflate(inflater, R.layout.teacher_list_item_classroom, parent, false));
        }

        @Override
        public void onBind(BindingHolder<? extends ViewDataBinding> holder, Classroom item, int position) {
            ViewDataBinding binding = holder.getBinding();
            if (binding instanceof TeacherListItemClassroomBinding) {
                TeacherListItemClassroomBinding b = (TeacherListItemClassroomBinding) binding;
                b.classroomName.setText(item.getClassName());
                b.classSchedule.setText(item.getClassScheduleAsText());
                b.subjectName.setText(item.getMainSubject());
                b.studentCount.setText(String.valueOf(item.getStudents().size()));
            }
        }
    }
}
