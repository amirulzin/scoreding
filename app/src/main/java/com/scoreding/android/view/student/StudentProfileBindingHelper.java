package com.scoreding.android.view.student;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;

import com.scoreding.android.Demo;
import com.scoreding.android.databinding.StudentProfileBinding;
import com.scoreding.android.model.composite.Student;
import com.scoreding.android.view.common.util.GlideUtil;
import com.scoreding.android.view.common.util.StringUtil;
import com.scoreding.android.view.teacher.TeacherPublicProfileActivity;

import java.lang.ref.WeakReference;

/**
 *
 */

public class StudentProfileBindingHelper {
    public static void applyStudent(final Context context, StudentProfileBinding binding, final Student student, boolean enableFab) {

        binding.profileFab.setVisibility(enableFab ? View.VISIBLE : View.GONE);

        binding.profileName.setText(student.getName());
        binding.className.setText(student.getClassroomName());
        binding.emoticons.setText(makeEmoticons(student));
        binding.attendanceStatus.setText(getAttendanceStatus(student));
        binding.homeworkStatus.setText(getHomeworkStatus(student));
        binding.latestComment.setText(student.getLatestComment());
        binding.teacher.setText(getTeacherString(student));
        final WeakReference<Context> weakRef = new WeakReference<>(context);
        binding.teacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = TeacherPublicProfileActivity.makeIntent(context, Demo.getMainTeacher(student.getHomeroomTeacher()));
                Context c = weakRef.get();
                if (c != null) {
                    c.startActivity(intent);
                }
            }
        });

        String imageUrl = student.getImageUrl();
        if (imageUrl != null)
            GlideUtil.loadProfileImage(binding.getRoot().getContext(), imageUrl, binding.profileImage);
    }

    @NonNull
    private static String getTeacherString(Student student) {
        return "Teacher:  ".concat(student.getHomeroomTeacher());
    }

    @NonNull
    private static String getHomeworkStatus(Student student) {
        return "Homework\n".concat(student.getHomeworkStatus());
    }

    @NonNull
    private static String getAttendanceStatus(Student student) {
        return "Attendance\n".concat(student.getAttendanceStatus());
    }


    private static String makeEmoticons(Student student) {
        return String.format("%s %s        %s %s", StringUtil.getEmojiSmile(), student.getPositiveFeedbacks(), StringUtil.getEmojiSad(), student.getNegativeFeedbacks());
    }

}
