package com.scoreding.android.view.common;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scoreding.android.R;
import com.scoreding.android.databinding.CommonFragmentRecyclerViewBinding;
import com.scoreding.android.databinding.ModuleListItemAnnouncementBinding;
import com.scoreding.android.model.composite.Event;
import com.scoreding.android.view.common.core.BaseFragment;
import com.scoreding.android.view.common.util.BindingHolder;
import com.scoreding.android.view.common.util.BindingItemAdapter;
import com.scoreding.android.view.common.util.ItemClickSupport;
import com.scoreding.android.view.teacher.EditEventActivity;

import org.parceler.Parcels;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 *
 */

public class AnnouncementFragment extends BaseFragment<CommonFragmentRecyclerViewBinding> {
    private static final String KEY_EVENTS = "EVENTS";
    private static final String KEY_FAB_ENABLED = "FAB_ENABLED";
    private List<Event> events;

    public static AnnouncementFragment newInstance(List<Event> events, boolean enableFab) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_EVENTS, Parcels.wrap(events));
        args.putBoolean(KEY_FAB_ENABLED, enableFab);
        AnnouncementFragment fragment = new AnnouncementFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private static Intent insertEvent(Event event) {
        Calendar beginTime = Calendar.getInstance();
        beginTime.setTime(event.getDate());

        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                //.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
                .putExtra(CalendarContract.Events.TITLE, event.getTitle())
                .putExtra(CalendarContract.Events.DESCRIPTION, event.getDetails())
                .putExtra(CalendarContract.Events.EVENT_LOCATION, event.getVenue())
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
        //.putExtra(Intent.EXTRA_EMAIL, "user@example.com, user@example.com");

        return intent;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.common_fragment_recycler_view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        CommonFragmentRecyclerViewBinding b = getBinding();
        b.title.setText("Announcement");

        events = Parcels.unwrap(getArguments().getParcelable(KEY_EVENTS));

        RecyclerView rv = b.recyclerView;
        rv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rv.setAdapter(new Adapter(events));

        if (getArguments().getBoolean(KEY_FAB_ENABLED)) {
            FloatingActionButton fab = b.fab;
            fab.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_mode_edit));
            fab.setVisibility(View.VISIBLE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivityForResult(EditEventActivity.createActivityIntent(getActivity(), null), EditEventActivity.getRequestCode());

                }
            });
        }

        ItemClickSupport.addTo(rv).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                startActivity(insertEvent(events.get(position)));
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Event event = EditEventActivity.readResult(getActivity(), requestCode, resultCode, data);
        if (event != null) {
            //TODO Minor = Implement binding with new Event created by Teacher
            events.add(event);
            RecyclerView rv = getBinding().recyclerView;
            int last = events.size() - 1;
            rv.getAdapter().notifyItemInserted(last);
            rv.scrollToPosition(last);
        }
    }

    public static class Adapter extends BindingItemAdapter<Event> {

        private final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d", Locale.getDefault());

        public Adapter(List<Event> list) {
            super(list);
        }

        @Override
        public BindingHolder<? extends ViewDataBinding> onCreateBindingHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
            return new BindingHolder<>(DataBindingUtil.inflate(inflater, R.layout.module_list_item_announcement, parent, false));
        }

        @Override
        public void onBind(BindingHolder<? extends ViewDataBinding> holder, Event item, int position) {
            ViewDataBinding binding = holder.getBinding();
            if (binding instanceof ModuleListItemAnnouncementBinding) {
                ModuleListItemAnnouncementBinding b = (ModuleListItemAnnouncementBinding) binding;
                b.eventTitle.setText(item.getTitle());
                b.eventDate.setText(dateFormat.format(item.getDate()));
                b.eventDetails.setText(item.getDetails());
            }
        }
    }
}
