package com.scoreding.android.view.parents;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import com.scoreding.android.Demo;
import com.scoreding.android.R;
import com.scoreding.android.view.common.AnnouncementFragment;
import com.scoreding.android.view.common.CalendarFragment;
import com.scoreding.android.view.common.HomeworkFragment;
import com.scoreding.android.view.common.core.BaseNavigationActivity;

import java.util.HashMap;

public class ParentHomeActivity extends BaseNavigationActivity {

    private final HashMap<Integer, String> fragmentTagsMap = new HashMap<Integer, String>(4, 1f) {
        {
            put(R.id.navigationHome, ParentProfileFragment.class.getSimpleName());
            put(R.id.navigationAcademic, HomeworkFragment.class.getSimpleName());
            put(R.id.navigationPlanner, CalendarFragment.class.getSimpleName());
            put(R.id.navigationAnnouncement, AnnouncementFragment.class.getSimpleName());
        }
    };

    @Override
    protected Fragment getInitialFragment() {
        return ParentProfileFragment.newInstance(Demo.createParentDemo());
    }

    @Override
    protected int getMenuResId() {
        return R.menu.navigation_parent;
    }

    @Override
    protected Fragment getFragment(MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.navigationHome:
                fragment = ParentProfileFragment.newInstance(Demo.createParentDemo());
                break;
            case R.id.navigationAcademic:
                fragment = HomeworkFragment.newInstance(Demo.createParentDemo().getStudents());
                break;
            case R.id.navigationPlanner:
                fragment = CalendarFragment.newInstance(Demo.getEvents());
                break;
            case R.id.navigationAnnouncement:
                fragment = AnnouncementFragment.newInstance(Demo.getEvents(), false);
                break;
        }

        return fragment;
    }

    @NonNull
    @Override
    protected String getFragmentTag(MenuItem item) {
        return fragmentTagsMap.get(item.getItemId());
    }

    @NonNull
    @Override
    protected String getInitialFragmentTag() {
        return fragmentTagsMap.get(R.id.navigationHome);
    }


}
