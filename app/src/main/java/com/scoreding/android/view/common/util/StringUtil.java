package com.scoreding.android.view.common.util;

/**
 *
 */

public class StringUtil {

    private static final int UNI_SMILE = 0x1F604;
    private static final int UNI_SAD = 0x1F61E;
    private static final String SMILE = getEmojiByUnicode(UNI_SMILE);
    private static final String SAD = getEmojiByUnicode(UNI_SAD);

    public static String getEmojiSad() {
        return SAD;
    }

    public static String getEmojiSmile() {

        return SMILE;
    }

    public static String getEmojiByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }
}
