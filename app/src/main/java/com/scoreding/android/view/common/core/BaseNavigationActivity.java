package com.scoreding.android.view.common.core;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.MenuRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.scoreding.android.R;
import com.scoreding.android.databinding.CommonActivityHomeBinding;

/**
 *
 */

public abstract class BaseNavigationActivity extends BaseBindingActivity<CommonActivityHomeBinding> {

    private Fragment currFragment;

    protected BottomNavigationView getBottomNavigation() {
        return getBaseBinding().bottomNavigation;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        BottomNavigationView bottomNavigation = getBaseBinding().bottomNavigation;
        bottomNavigation.inflateMenu(getMenuResId());
        bottomNavigation.setOnNavigationItemSelectedListener(getNavListener());

        FragmentManager fm = getSupportFragmentManager();
        if (currFragment == null) {

            currFragment = getInitialFragment();
            fm.beginTransaction()
                    .add(getContentContainerId(), currFragment, getInitialFragmentTag())
                    .commit();

        }
    }

    protected int getBaseLayoutId() {
        return R.layout.common_activity_home;
    }


    protected abstract Fragment getInitialFragment();

    @IdRes
    protected int getContentContainerId() {
        return getBaseBinding().content.getId();
    }

    protected FrameLayout getContentContainer() {
        return getBaseBinding().content;
    }

    @MenuRes
    protected abstract int getMenuResId();

    protected BottomNavigationView.OnNavigationItemSelectedListener getNavListener() {
        return new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                if (item.getItemId() == getBottomNavigation().getSelectedItemId())
                    return false;

                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction transaction = fm.beginTransaction();

                transaction.hide(currFragment);
                String tag = getFragmentTag(item);
                Fragment foundFragment = fm.findFragmentByTag(tag);
                if (foundFragment == null) {
                    Fragment f = getFragment(item);
                    if (f != null) {
                        transaction.add(getContentContainerId(), f, tag);
                        currFragment = f;
                    }
                } else {
                    currFragment = foundFragment;
                }
                transaction.show(currFragment);
                transaction.commit();
                return true;
            }
        };
    }

    @Nullable
    protected abstract Fragment getFragment(MenuItem item);

    @NonNull
    protected abstract String getFragmentTag(MenuItem item);

    @NonNull
    protected abstract String getInitialFragmentTag();
}
