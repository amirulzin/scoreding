package com.scoreding.android.view.teacher;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.scoreding.android.R;
import com.scoreding.android.databinding.TeacherActivityEditEventBinding;
import com.scoreding.android.model.composite.Event;
import com.scoreding.android.view.common.core.BaseBindingActivity;

import org.parceler.Parcels;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 *
 */

public class EditEventActivity extends BaseBindingActivity<TeacherActivityEditEventBinding> implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private static final int KEY_REQUEST_CODE = 42;
    private static final String KEY_OUT_EVENT = "OUT_EVENT";
    private static final String KEY_IN_EVENT = "IN_EVENT";
    private final SimpleDateFormat tf = new SimpleDateFormat("h:mm a", Locale.getDefault());
    private final SimpleDateFormat df = new SimpleDateFormat("d MMM yy", Locale.getDefault());
    private final GregorianCalendar calendarOut = getDefaultCal();

    public static int getRequestCode() {
        return KEY_REQUEST_CODE;
    }

    public static Intent createActivityIntent(Context context, Event nullableInput) {
        Intent intent = new Intent(context, EditEventActivity.class);
        if (nullableInput != null)
            intent.putExtra(KEY_IN_EVENT, Parcels.wrap(nullableInput));
        return intent;
    }

    @Nullable
    public static Event readResult(Context context, int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_CANCELED) return null;
        if (resultCode == RESULT_OK && requestCode == KEY_REQUEST_CODE) {
            Event event = Parcels.unwrap(data.getParcelableExtra(KEY_OUT_EVENT));
            Toast.makeText(context.getApplicationContext(), "Event added: " + event.getTitle(), Toast.LENGTH_LONG).show();
            return event;
        }
        return null;
    }

    @Override
    protected int getBaseLayoutId() {
        return R.layout.teacher_activity_edit_event;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TeacherActivityEditEventBinding b = getBaseBinding();

        Parcelable p = getIntent().getParcelableExtra(KEY_IN_EVENT);
        Event event = null;
        if (p != null) {
            event = Parcels.unwrap(p);
            b.eventTitle.setText(event.getTitle());
            b.eventVenue.setText(event.getVenue());
            b.eventDetails.setText(event.getDetails());
        }


        Date calTime = calendarOut.getTime();
        b.eventTime.setText(tf.format(calTime));
        b.eventTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog dialog = new TimePickerDialog(EditEventActivity.this, EditEventActivity.this, calendarOut.get(Calendar.HOUR_OF_DAY), calendarOut.get(Calendar.MINUTE), false);
                dialog.show();
            }
        });
        b.eventDate.setText(df.format(calTime));
        b.eventDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dialog = new DatePickerDialog(EditEventActivity.this, EditEventActivity.this, calendarOut.get(Calendar.YEAR), calendarOut.get(Calendar.MONTH), calendarOut.get(Calendar.DAY_OF_MONTH));
                dialog.show();
            }
        });

        b.actionCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        b.actionSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out = new Intent();
                TeacherActivityEditEventBinding b = getBaseBinding();

                Event outEvent = new Event(b.eventTitle.getText().toString(), b.eventDetails.getText().toString(), b.eventVenue.getText().toString(), getEventType(), calendarOut.getTimeInMillis());

                out.putExtra(KEY_OUT_EVENT, Parcels.wrap(outEvent));
                setResult(RESULT_OK, out);
                finish();
            }
        });
    }

    private String getEventType() { //TODO Minor : Assign Event type by radio button type
        int radioButtonId = getBaseBinding().eventRadioGroup.getCheckedRadioButtonId();

        return null;
    }

    private GregorianCalendar getDefaultCal() {
        final GregorianCalendar cal = new GregorianCalendar(Locale.getDefault());
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 9, 0);
        cal.set(Calendar.SECOND, 0);
        return cal;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        calendarOut.set(year, month, dayOfMonth);
        getBaseBinding().eventDate.setText(df.format(calendarOut.getTime()));
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        calendarOut.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendarOut.set(Calendar.MINUTE, hourOfDay);
        getBaseBinding().eventTime.setText(tf.format(calendarOut.getTime()));
    }
}
