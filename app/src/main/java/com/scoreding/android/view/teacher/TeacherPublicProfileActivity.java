package com.scoreding.android.view.teacher;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.scoreding.android.R;
import com.scoreding.android.databinding.CommonFragmentProfileBinding;
import com.scoreding.android.model.composite.Teacher;
import com.scoreding.android.view.common.core.BaseBindingActivity;

import org.parceler.Parcels;

/**
 * Activity used when viewed from Parent or Student account
 */

public class TeacherPublicProfileActivity extends BaseBindingActivity<CommonFragmentProfileBinding> {
    private static final String KEY_TEACHER = "TEACHER";

    public static Intent makeIntent(Context context, Teacher teacher) {
        Intent intent = new Intent(context, TeacherPublicProfileActivity.class);
        intent.putExtra(KEY_TEACHER, Parcels.wrap(teacher));
        return intent;
    }

    @Override
    protected int getBaseLayoutId() {
        return R.layout.common_fragment_profile;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Teacher teacher = Parcels.unwrap(getIntent().getParcelableExtra(KEY_TEACHER));

        TeacherProfileFragment fragment = TeacherProfileFragment.newInstance(teacher, false, false);
        getSupportFragmentManager()
                .beginTransaction()
                .add(getBaseBinding().fragmentContainer.getId(), fragment, TeacherProfileFragment.class.getSimpleName()).commit();
    }
}
