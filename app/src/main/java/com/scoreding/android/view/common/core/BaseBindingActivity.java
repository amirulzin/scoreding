package com.scoreding.android.view.common.core;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 *
 */

public abstract class BaseBindingActivity<T extends ViewDataBinding> extends AppCompatActivity {

    private T binding;

    protected abstract int getBaseLayoutId();

    protected T getBaseBinding() {
        return binding;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getBaseLayoutId());
    }
}
