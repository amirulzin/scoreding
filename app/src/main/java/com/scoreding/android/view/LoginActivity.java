package com.scoreding.android.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.scoreding.android.Demo;
import com.scoreding.android.R;
import com.scoreding.android.databinding.CommonActivityLoginBinding;
import com.scoreding.android.view.parents.ParentHomeActivity;
import com.scoreding.android.view.student.StudentHomeActivity;
import com.scoreding.android.view.teacher.TeacherHomeActivity;

public class LoginActivity extends AppCompatActivity {

    private CommonActivityLoginBinding contentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentView = DataBindingUtil.setContentView(this, R.layout.common_activity_login);
        contentView.loginRadiogroup.check(R.id.login_radbut_parents);
        contentView.loginEmail.setText(Demo.getDemoEmail());
        contentView.loginPassword.setText(Demo.getDemoPassword());
    }

    public void attemptLogin(View view) {
        int id = contentView.loginRadiogroup.getCheckedRadioButtonId();

        String email = contentView.loginEmail.getText().toString();
        String password = contentView.loginPassword.getText().toString();
        if (id == R.id.login_radbut_parents) {
            startActivity(new Intent(this, ParentHomeActivity.class));
            finish();
        } else if (id == R.id.login_radbut_teachers) {
            startActivity(new Intent(this, TeacherHomeActivity.class));
            finish();
        } else if (id == R.id.login_radbut_students) {
            startActivity(new Intent(this, StudentHomeActivity.class));
            finish();
        }

    }
}
